Index: mdanalysis/package/MDAnalysis/coordinates/GSD.py
===================================================================
--- mdanalysis.orig/package/MDAnalysis/coordinates/GSD.py	2023-07-28 12:30:08.996381416 +0200
+++ mdanalysis/package/MDAnalysis/coordinates/GSD.py	2023-07-28 12:30:08.984381275 +0200
@@ -51,9 +51,21 @@
 
 """
 import numpy as np
-import gsd
-import gsd.fl
-import gsd.hoomd
+try:
+    import gsd
+    import gsd.fl
+except ImportError:
+    HAS_GSD = False
+    import types
+
+    class MockHOOMDTrajectory:
+        pass
+
+    gsd = types.ModuleType("gsd")
+    gsd.hoomd = types.ModuleType("hoomd")
+    gsd.hoomd.HOOMDTrajectory = MockHOOMDTrajectory
+else:
+    HAS_GSD = True
 
 from . import base
 from MDAnalysis.lib.util import store_init_arguments
@@ -85,6 +97,10 @@
            Support for GSD versions below 3.0.1 have been dropped. This
            includes support for schema 1.3.
         """
+        if not HAS_GSD:
+            errmsg = "GSDReader: To read GSD files, please install gsd"
+            raise ImportError(errmsg)
+
         super(GSDReader, self).__init__(filename, **kwargs)
         self.filename = filename
         self.open_trajectory()
Index: mdanalysis/package/MDAnalysis/topology/GSDParser.py
===================================================================
--- mdanalysis.orig/package/MDAnalysis/topology/GSDParser.py	2023-07-28 12:30:08.996381416 +0200
+++ mdanalysis/package/MDAnalysis/topology/GSDParser.py	2023-07-28 12:30:08.984381275 +0200
@@ -52,7 +52,6 @@
 
 """
 import os
-import gsd.hoomd
 import numpy as np
 
 from . import guessers
@@ -75,6 +74,13 @@
     Segids,
 )
 
+try:
+    import gsd.hoomd
+except ImportError:
+    HAS_GSD = False
+else:
+    HAS_GSD = True
+
 
 class GSDParser(TopologyReaderBase):
     """Parses a Hoomd GSD file to create a Topology
@@ -101,6 +107,14 @@
     """
     format = 'GSD'
 
+    def __init__(self, filename):
+
+        if not HAS_GSD:
+            errmsg = ("GSDParser: To read a Topology from a Hoomd GSD "
+                      "file, please install gsd")
+            raise ImportError(errmsg)
+        super(GSDParser, self).__init__(filename)
+
     def parse(self, **kwargs):
         """Parse Hoomd GSD file
 
Index: mdanalysis/package/pyproject.toml
===================================================================
--- mdanalysis.orig/package/pyproject.toml	2023-07-28 12:30:08.996381416 +0200
+++ mdanalysis/package/pyproject.toml	2023-07-28 12:30:08.984381275 +0200
@@ -80,6 +80,7 @@
     "chemfiles>=0.10",
     "pyedr>=0.7.0",
     "pytng>=0.2.3",
+    "gsd>=1.9.3,<3.0",
 ]
 analysis = [
     "seaborn",
Index: mdanalysis/package/setup.py
===================================================================
--- mdanalysis.orig/package/setup.py	2023-07-28 12:30:08.996381416 +0200
+++ mdanalysis/package/setup.py	2023-07-28 12:30:08.984381275 +0200
@@ -654,6 +654,7 @@
                   'pytng>=0.2.3',  # TNG
                   'chemfiles>=0.10',  # multiple formats supported by chemfiles
                   'pyedr>=0.7.0',  # EDR files for the EDR AuxReader
+                  'gsd>=1.9.3,<3.0', # GSD
                   ],
               'analysis': [
                   'seaborn',  # for annotated heat map and nearest neighbor
Index: mdanalysis/testsuite/MDAnalysisTests/coordinates/test_copying.py
===================================================================
--- mdanalysis.orig/testsuite/MDAnalysisTests/coordinates/test_copying.py	2023-07-28 12:30:08.996381416 +0200
+++ mdanalysis/testsuite/MDAnalysisTests/coordinates/test_copying.py	2023-07-28 12:30:08.988381323 +0200
@@ -60,6 +60,7 @@
     XYZ_mini,
 )
 from MDAnalysis.coordinates.core import get_reader_for
+from MDAnalysis.coordinates.GSD import HAS_GSD
 from MDAnalysis.auxiliary.EDR import HAS_PYEDR
 
 
@@ -89,7 +90,10 @@
     ('memory', np.arange(60).reshape(2, 10, 3).astype(np.float64), dict()),
     ('CHAIN', [GRO, GRO, GRO], dict()),
     ('FHIAIMS', FHIAIMS, dict()),
-    ('GSD', GSD, dict()),
+    pytest.param(
+        ('GSD', GSD, dict()),
+        marks=pytest.mark.skipif(not HAS_GSD, reason='gsd not installed')
+    ),
     ('NAMDBIN', NAMDBIN, dict()),
     ('TXYZ', TXYZ, dict()),
 ])
@@ -130,7 +134,10 @@
     ('memory', np.arange(60).reshape(2, 10, 3).astype(np.float64), dict()),
     ('CHAIN', [GRO, GRO, GRO], dict()),
     ('FHIAIMS', FHIAIMS, dict()),
-    ('GSD', GSD, dict()),
+    pytest.param(
+        ('GSD', GSD, dict()),
+        marks=pytest.mark.skipif(not HAS_GSD, reason='gsd not installed')
+    ),
     ('NAMDBIN', NAMDBIN, dict()),
     ('TXYZ', TXYZ, dict()),
 ])
Index: mdanalysis/testsuite/MDAnalysisTests/coordinates/test_gsd.py
===================================================================
--- mdanalysis.orig/testsuite/MDAnalysisTests/coordinates/test_gsd.py	2023-07-28 12:30:08.996381416 +0200
+++ mdanalysis/testsuite/MDAnalysisTests/coordinates/test_gsd.py	2023-07-28 12:30:08.988381323 +0200
@@ -26,36 +26,48 @@
 from numpy.testing import assert_almost_equal
 
 import MDAnalysis as mda
-from MDAnalysis.coordinates.GSD import GSDReader
+from MDAnalysis.coordinates.GSD import GSDReader, HAS_GSD
 from MDAnalysisTests.datafiles import GSD
 
 
-@pytest.fixture
-def GSD_U():
-    return mda.Universe(GSD)
-
-
-def test_gsd_positions(GSD_U):
-    # first frame first particle
-    ts = GSD_U.trajectory[0]
-    assert_almost_equal(GSD_U.atoms.positions[0],
-                        [ -5.4000001 , -10.19999981, -10.19999981])
-    # second frame first particle
-    ts = GSD_U.trajectory[1]
-    assert_almost_equal(GSD_U.atoms.positions[0],
-                        [ -5.58348083,  -9.98546982, -10.17657185])
-
-
-def test_gsd_n_frames(GSD_U):
-    assert len(GSD_U.trajectory) == 2
-
-
-def test_gsd_dimensions(GSD_U):
-    ts = GSD_U.trajectory[0]
-    assert_almost_equal(ts.dimensions,
-                        [ 21.60000038,21.60000038,21.60000038,90.,90.,90.])
-
-
-def test_gsd_data_step(GSD_U):
-    assert GSD_U.trajectory[0].data['step'] == 0
-    assert GSD_U.trajectory[1].data['step'] == 500
+@pytest.mark.skipif(HAS_GSD, reason='gsd is installed')
+def test_no_gsd_u_raises():
+    with pytest.raises(ImportError, match='please install gsd'):
+        _ = mda.Universe(GSD)
+
+
+@pytest.mark.skipif(HAS_GSD, reason='gsd is installed')
+def test_gsd_reader_raises():
+    with pytest.raises(ImportError, match='please install gsd'):
+        _ = GSDReader('foo')
+
+
+@pytest.mark.skipif(not HAS_GSD, reason='gsd is not installed')
+class TestGSDReader:
+    @pytest.fixture(scope='class')
+    def GSD_U(self):
+        return mda.Universe(GSD)
+
+    def test_gsd_positions(self, GSD_U):
+        # first frame first particle
+        GSD_U.trajectory[0]
+        assert_almost_equal(GSD_U.atoms.positions[0],
+                            [-5.4000001 , -10.19999981, -10.19999981])
+        # second frame first particle
+        GSD_U.trajectory[1]
+        assert_almost_equal(GSD_U.atoms.positions[0],
+                            [-5.58348083,  -9.98546982, -10.17657185])
+    
+    def test_gsd_n_frames(self, GSD_U):
+        assert len(GSD_U.trajectory) == 2
+
+    def test_gsd_dimensions(self, GSD_U):
+        ts = GSD_U.trajectory[0]
+        assert_almost_equal(
+            ts.dimensions,
+            [21.60000038, 21.60000038, 21.60000038, 90., 90., 90.]
+        )
+
+    def test_gsd_data_step(self, GSD_U):
+        assert GSD_U.trajectory[0].data['step'] == 0
+        assert GSD_U.trajectory[1].data['step'] == 500
Index: mdanalysis/testsuite/MDAnalysisTests/parallelism/test_multiprocessing.py
===================================================================
--- mdanalysis.orig/testsuite/MDAnalysisTests/parallelism/test_multiprocessing.py	2023-07-28 12:30:08.996381416 +0200
+++ mdanalysis/testsuite/MDAnalysisTests/parallelism/test_multiprocessing.py	2023-07-28 12:30:08.988381323 +0200
@@ -32,6 +32,7 @@
 import MDAnalysis as mda
 import MDAnalysis.coordinates
 from MDAnalysis.coordinates.core import get_reader_for
+from MDAnalysis.coordinates.GSD import HAS_GSD
 from MDAnalysis.analysis.rms import RMSD
 
 from MDAnalysisTests.datafiles import (
@@ -73,7 +74,10 @@
     (XYZ_bz2,),  # .bz2
     (GMS_SYMOPT,),  # .gms
     (GMS_ASYMOPT,),  # .gz
-    (GSD_long,),
+    pytest.param(
+        (GSD_long,),
+        marks=pytest.mark.skipif(not HAS_GSD, reason='gsd not installed')
+    ),
     (NCDF,),
     (np.arange(150).reshape(5, 10, 3).astype(np.float64),),
     (GRO, [GRO, GRO, GRO, GRO, GRO]),
@@ -171,7 +175,10 @@
     ('LAMMPSDUMP', LAMMPSDUMP, dict()),
     ('GMS', GMS_ASYMOPT, dict()),
     ('GRO', GRO, dict()),
-    ('GSD', GSD, dict()),
+    pytest.param(
+        ('GSD', GSD, dict()),
+        marks=pytest.mark.skipif(not HAS_GSD, reason='gsd not installed')
+    ),
     ('MMTF', MMTF, dict()),
     ('MOL2', mol2_molecules, dict()),
     ('PDB', PDB_small, dict()),
Index: mdanalysis/testsuite/MDAnalysisTests/topology/test_gsd.py
===================================================================
--- mdanalysis.orig/testsuite/MDAnalysisTests/topology/test_gsd.py	2023-07-28 12:30:08.996381416 +0200
+++ mdanalysis/testsuite/MDAnalysisTests/topology/test_gsd.py	2023-07-28 12:30:08.988381323 +0200
@@ -23,6 +23,7 @@
 import pytest
 
 import MDAnalysis as mda
+from MDAnalysis.topology.GSDParser import HAS_GSD
 
 from MDAnalysisTests.topology.base import ParserBase
 from MDAnalysisTests.datafiles import GSD
@@ -30,6 +31,8 @@
 from numpy.testing import assert_equal
 import os
 
+
+@pytest.mark.skipif(not HAS_GSD, reason='gsd not installed')
 class GSDBase(ParserBase):
     parser = mda.topology.GSDParser.GSDParser
     expected_attrs = ['ids', 'names', 'resids', 'resnames', 'masses',
@@ -78,14 +81,15 @@
             assert top.impropers.values == []
 
 
+@pytest.mark.skipif(not HAS_GSD, reason='gsd not installed')
 class TestGSDParser(GSDBase):
     ref_filename = GSD
     expected_n_atoms = 5832
     expected_n_residues = 648
     expected_n_segments = 1
 
-    
 
+@pytest.mark.skipif(not HAS_GSD, reason='gsd not installed')
 class TestGSDParserBonds(GSDBase):
     ref_filename = GSD_bonds
     expected_n_atoms = 490
Index: mdanalysis/testsuite/MDAnalysisTests/utils/test_pickleio.py
===================================================================
--- mdanalysis.orig/testsuite/MDAnalysisTests/utils/test_pickleio.py	2023-07-28 12:30:08.996381416 +0200
+++ mdanalysis/testsuite/MDAnalysisTests/utils/test_pickleio.py	2023-07-28 12:30:08.988381323 +0200
@@ -38,7 +38,8 @@
 )
 from MDAnalysis.coordinates.GSD import (
     GSDPicklable,
-    gsd_pickle_open
+    gsd_pickle_open,
+    HAS_GSD
 )
 from MDAnalysis.coordinates.TRJ import (
     NCDFPicklable,
@@ -161,6 +162,7 @@
         f_pickled = pickle.loads(pickle.dumps(f_open_by_class))
 
 
+@pytest.mark.skipif(not HAS_GSD, reason='gsd not installed')
 def test_GSD_pickle():
     gsd_io = gsd_pickle_open(GSD, mode='r')
     gsd_io_pickled = pickle.loads(pickle.dumps(gsd_io))
@@ -168,6 +170,7 @@
                  gsd_io_pickled[0].particles.position)
 
 
+@pytest.mark.skipif(not HAS_GSD, reason='gsd not installed')
 def test_GSD_with_write_mode(tmpdir):
     with pytest.raises(ValueError, match=r"Only read mode"):
         gsd_io = gsd_pickle_open(tmpdir.mkdir("gsd").join('t.gsd'),
